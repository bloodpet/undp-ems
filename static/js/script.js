/**
 * Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see 
 * http://www.gnu.org/licenses/agpl-3.0.html.
 *
 */

function open_in_popup(url) {
	//var new_win = window.open(url, 'xaasy-popup', 'scrollbars=yes,width=1280,height=1024');
	var new_win = window.open(url, 'xaasy-popup', 'scrollbars=yes,width=1680,height=1050');
	//var new_win = window.open(url, 'xaasy-popup', 'scrollbars=yes,fullscreen=yes');
	if (window.focus) {new_win.focus()};
	return false;
};


$(function(){

	function custom_ui() {
		$('*[role="button"]').button();
		$('input[type="submit"]').button();
		//$('a').button();
		$('form label').each(function (e) {
			var label = $(this),
				input = $('#' + label.attr('for'));
			if (! input.attr('placeholder'))
				input.attr('placeholder', label.text());
		});
		$('select').chosen();
		$('thead').addClass('ui-widget-header');
		//$('tbody').addClass('ui-widget-content');
		$('.data-table *[role="button"]').css({
			'fontSize': '12px',
		});
		$('.shortcut *[role="button"]').css({
			'fontSize': '10px',
		});
	};

	function open_link_in_popup() {
		var url = $(this).attr('href');
		console.log('Opening ' + url);
		return open_in_popup(url);
	};

	function open_form_in_popup() {
		var form = $(this);
		console.log(form.children('.required').val());
		if (form.children('.required').val()) {
			var url = form.attr('action') + '?' + form.serialize();
			console.log('Opening ' + url);
			return open_in_popup(url);
		}
		console.log('Illegal input');
		return false;
	};

	function remove_entries() {
		console.log('Clicked remove.');
		$('#action-input').attr('value', 'remove');
		$('#table-form').submit();
		return false;
	};

	function release_entries() {
		console.log('Clicked release.');
		$('#action-input').attr('value', 'release');
		$('#table-form').submit();
		return false;
	};

	function mark_false() {
		console.log('Clicked release.');
		$('#action-input').attr('value', 'mark_false');
		$('#table-form').submit();
		return false;
	};

	$('.close').click(function () {
		window.close();
		return false;
	});

	$('.create-from-alert').click(function () {
		console.log('Clicked create from alert(s).');
		$('#action-input').attr('value', 'alert');
		var values = $('#table-form').serialize();
		open_in_popup('/create_associated/?' + values);
		//window.location.reload();
		return false;
	});

	$('.remove-entries').click(remove_entries);
	$('.remove-entry').click(function () {
		var ret = remove_entries();
		window.close();
		return ret;
	});

	$('.release-entries').click(release_entries);
	$('.release-entry').click(function () {
		var ret = release_entries();
		window.close();
		return ret;
	});

	$('.false-entries').click(mark_false);
	$('.false-entry').click(function () {
		var ret = mark_false();
		window.close();
		return ret;
	});

	custom_ui();
	$('.popup').click(open_link_in_popup);
	$('.popup-form').submit(open_form_in_popup);

});
