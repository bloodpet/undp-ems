/**
 * Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see 
 * http://www.gnu.org/licenses/agpl-3.0.html.
 *
 */


$(function(){

	if (window.opener)
		console.log('form.js Parent: ' + window.opener.location);
	$(window).blur(function() {
		if (window.opener) window.opener.location.reload();
	});
	$('input[type="time"]').timepicker({
		minutes: {
			ends: 59,
			interval: 1,
			starts: 0
		},
		'dateformat': 'H:i'
	});
	$('input[type="date"]').datepicker({
		//currentText: 'Now',
		dateFormat: 'yy-mm-dd',
		//defaultDate: new Date,
		//defaultDate: +0,
		firstDay: 1,
		//gotoCurrent: true,
		// Change this based on the language
		//monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		showButtonPanel: true,
		showMonthAfterYear: true,
		yearSuffix: ''
	});

	$('form.autosave').autosave({
		save: {
			method: 'ajax',
			options: {
				success: function () {
					console.log('Saving form');
				}
			}
		}
	});

});
