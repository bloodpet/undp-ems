/**
 * Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see 
 * http://www.gnu.org/licenses/agpl-3.0.html.
 *
 */


$(function(){
	var $district = $('#id_district');
	var $county = $('#id_county');
	var $locality = $('#id_locality');
	var $districtChzn = $('#id_district_chzn');
	var $countyChzn = $('#id_county_chzn');
	var $localityChzn = $('#id_locality_chzn');
	var distPatt = /^[0-9][0-9]/;
	var districtVal = $district.val();
	var countyVal = $county.val();

	var filterCounties = function(){
		var chosenTxt = $district.children(
			'option[value=' + districtVal + ']').text();
		var chosenNum = chosenTxt.substr(0, 2);
		$('#id_county_chzn li').each(function(i, e){
			if ($(e).text().indexOf('-') == 0 ||
				$(e).text().indexOf(chosenNum) == 0)
				$(e).show();
			else
				$(e).hide();
		});
		return true;
	};

	var filterLocalities = function(){
		var chosenTxt = $county.children(
			'option[value=' + countyVal + ']').text();
		var chosenNum = chosenTxt.substr(0, 4);
		$('#id_locality_chzn li').each(function(i, e){
			if ($(e).text().indexOf('-') == 0 ||
				$(e).text().indexOf(chosenNum) == 0)
				$(e).show();
			else
				$(e).hide();
		});
		return true;
	};

	filterCounties();
	filterLocalities();
	$district.change(function(){
		if (districtVal != $district.val()) {
			districtVal = $district.val();
			// Set county and locality to None
			$('#id_county_chzn .chzn-single span').text(
				$('#id_county_chzn_o_0').text()
			);
			$('#id_locality_chzn .chzn-single span').text(
				$('#id_locality_chzn_o_0').text()
			);
			$county.val('');
			$locality.val('');
			$.get('?county=&locality=&district=' + districtVal);
			return filterCounties();
		} else {
			return false;
		}
	});
	$county.change(function(){
		if (countyVal != $county.val()) {
		    countyVal = $county.val();
			// Set locality to None
			$('#id_locality_chzn .chzn-single span').text(
				$('#id_locality_chzn_o_0').text()
			);
			$locality.val('');
			$.get('?locality=&county=' + countyVal);
			return filterLocalities();
		} else {
			return false;
		}
	});

});
