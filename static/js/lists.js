/**
 * Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see 
 * http://www.gnu.org/licenses/agpl-3.0.html.
 *
 */


$(function(){
	
	function tie_up_links() {
		$('.data-table tr').each(function () {
				$(this).children(':last').hide();
		});
		$('.data-table tbody tr td').click(function () {
				console.log('Clicked a td');
				var tr = $(this).parent();
				var kids = $(tr).children('td');
				var last_kid = kids[kids.length - 1];
				var link = $(last_kid).children('a.detail-link');
				var url = link.attr('href');
				return open_in_popup(url);
		});
	};


	$('.data-table').tablesorter();
	tie_up_links();

	$('.associate-alerts').click(function () {
		console.log('Clicked create from alert(s).');
		$('#action-input').attr('value', 'alert');
		var values = $('#table-form').serialize();
		open_in_popup('/associate_alerts/?' + values);
		return false;
	})

	$('.associate-occurrences').click(function () {
		console.log('Clicked create from occurrence(s).');
		$('#action-input').attr('value', 'occurrence');
		var values = $('#table-form').serialize();
		open_in_popup('/associate_occurrences/?' + values);
		return false;
	})

	$('.disassociate-entries').click(function () {
		console.log('Clicked disassociate entries.');
		$('#action-input').attr('value', 'disassociate');
		$('#table-form').submit();
		return false;
	});

});
