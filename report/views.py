__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@bloodpet.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


import logging
from django.views.generic import *


log = logging.getLogger('xaasy')


class EndShiftReport(TemplateView):
    template_name = 'reports/end_shift.html'

    def get_context_data(self, **kwargs):
        context_data = super(self.__class__, self).get_context_data(**kwargs)
        logs = self.request.user.logentry_set.filter(
            action_time__gt=self.request.user.last_login)
            #action_time__gt=self.request.user.last_login).distinct('object_id')
        context_data['alert_list'] = set([
            a.get_edited_object()
            for a in logs.filter(
            content_type__name='alert')])
        context_data['occurrence_list'] = set([
            a.get_edited_object()
            for a in logs.filter(
            content_type__name='occurrence')])
        return context_data
