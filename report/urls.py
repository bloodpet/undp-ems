__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@bloodpet.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required

from report.views import *


urlpatterns = patterns(
    'report.views',
    url(r'^end_shift/$', login_required(EndShiftReport.as_view()),
        {'template': 'reports/end_shift.html'}, name='end_shift'),
)
