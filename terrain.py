__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@bloodpet.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from lettuce import *
from django.test.client import Client
from django.core.urlresolvers import reverse
from lxml.html import soupparser


def get_url_path(args):
    args = args.split(' ')
    name = args.pop(0)
    return reverse(name, args=args)


@before.all
@after.all
def remove_entries(*args):
    '''Remove unnecessary entries before the test here.'''
    from incident.models import *
    for model in (
        Alert,
        Occurrence,
        Resource,
        DispatchRequest,
        Dispatch,
    ):
        pass
        #for entry in model.objects.all():
            #entry.delete()


@before.all
def set_browser():
    world.browser = Client()


@step(r'visit "(.*)"')
def visit(step, args):
    path = get_url_path(args)
    world.response = world.browser.get(path)
    world.dom = soupparser.fromstring(world.response.content)
    if world.response.status_code != 200:
        raise Exception(
            'Got status code %s when visiting the site.' %
            world.response.status_code)


@step(u'login as "([^"]*)" with password "([^"]*)"')
def login(step, username, password):
    world.browser.login(username=username, password=password)


@step(r'the header "(.*)"')
def see_header(step, text):
    header = world.dom.cssselect('h2')[0]
    assert header.text == text


@step(r'the subhead "(.*)"')
def see_subhead(step, text):
    header = world.dom.cssselect('h3')[0]
    assert header.text == text, '"%s" h3 Header is not "%s"' % (
        header.text, text)


@step(r'see the #([1-9][0-9]*) "(.*)" element with text "(.*)"')
def see_element(step, num, el, text):
    try:
        element = world.dom.cssselect(el)[int(num) - 1]
    except IndexError:
        raise AssertionError,\
                'The #{0} element {1} does not exist.'.format(num, el)
    else:
        assert element.text.strip() == text, '%s is not equal to %s' % (
            element.text.strip(), text)


@step(u'see a link to "([^"]*)"')
def see_link_to(step, args):
    path = get_url_path(args)
    el = '.main a[href="%s"]' % path
    elements = world.dom.cssselect(el)
    assert len(elements) > 0, 'There is no link to %s' % path


@step(u'see a link in the content to "([^"]*)"')
def see_link_to(step, args):
    path = get_url_path(args)
    el = '#main a[href="%s"]' % path
    elements = world.dom.cssselect(el)
    assert len(elements) > 0, 'There is no link to %s' % path


@step(u'see the form with id "([^"]*)"')
def see_form_id(step, form_id):
    elements = world.dom.cssselect('form#%s' % form_id)
    assert len(elements) > 0, 'There is no form with id %s' % form_id


@step(u'"([^"]*)" form has fields:')
def form_has_fields(step, form_id):
    form = world.dom.cssselect('form#%s' % form_id)[0]
    for field in step.hashes:
        if field['type'] in ('text', 'date'):
            els = form.cssselect('input[name="%s"]' % (field['name']))
            assert len(els) > 0,\
                    'There is no field with name "%s"' % field['name']
            el = els[0]
            assert el.type == 'text',\
                    '%s field uses an incorrect type %s' % (
                        field['name'], el.type)
        elif field['type'] == 'dropdown':
            els = form.cssselect('select[name="%s"]' % (field['name']))
            assert len(els) > 0,\
                    'There is no field with name "%s"' % field['name']
            el = els[0]


@step(u'submit "([^"]*)" form with values:')
def submit_form_with_values(step, form_id):
    form = world.dom.cssselect('form#%s' % form_id)[0]
    csrf = form.cssselect('input[name="csrfmiddlewaretoken"]')[0]
    data = {}
    for field in step.hashes:
        data[field['name']] = field['value']
    data['csrfmiddlewaretoken'] = csrf.value
    world.response = world.browser.post(
        form.attrib.get('action', world.response.request['PATH_INFO']),
        data)
    world.dom = soupparser.fromstring(world.response.content)
    if world.response.status_code != 200:
        Exception('Failed to submit data')


@step(u'check if entry is saved')
def check_if_entry_is_saved(step):
    errors = world.dom.cssselect('ul.errorlist li')
    for error in errors:
        raise Exception(error.text)


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
