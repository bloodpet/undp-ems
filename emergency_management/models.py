from django.db.models import *


class NamedModel(Model):
    class Meta:
        abstract = True

    def __unicode__(self):
        return self.name


class AbbreviatedModel(Model):
    class Meta:
        abstract = True

    def __unicode__(self):
        return self.abbr


class SerializedModel(Model):
    class Meta:
        abstract = True

    def __unicode__(self):
        txt = '#{0}'.format(self.number, '')
        #TODO Add location information
        return txt

    def get_location(self):
        return '%s %s %s' % (
            self.district or '',
            self.county or '',
            self.locality or ''
        )

    def save(self, *args, **kwargs):
        if self.number == 0:
            try:
                biggest = self.__class__.objects.latest('number').number
                self.number = biggest + 1
            except self.__class__.DoesNotExist:
                self.number = 1
        return super(SerializedModel, self).save(*args, **kwargs)
