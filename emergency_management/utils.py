from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import force_unicode


def log_entry(request, obj, action):
    LogEntry.objects.log_action(
        user_id=request.user.pk,
        content_type_id=ContentType.objects.get_for_model(obj).pk,
        object_id=obj.pk,
        object_repr=force_unicode(obj),
        action_flag=action,
    )


def add_entry(request, obj):
    log_entry(request, obj, ADDITION)


def edit_entry(request, obj):
    log_entry(request, obj, CHANGE)


def del_entry(request, obj):
    log_entry(request, obj, DELETION)
