from django.conf.urls import patterns, include, url

from django.contrib.auth import views as auth_views
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',

    # Examples:
    # url(r'^$', 'emergency_management.views.home', name='home'),
    # url(r'^emergency_management/', include('emergency_management.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    #url(r'^rosetta/', include('rosetta.urls')),

    url(r'^accounts/login/$', auth_views.login,
        {'template_name': 'login.html'}, name='auth_login'),
    url(r'^accounts/logout/$', auth_views.logout,
        {'template_name': 'login.html'}, name='auth_logout'),
    url(r'^translate/', include('rosetta.urls')),
    url(r'^reports/', include('report.urls')),
    url(r'^', include('incident.urls')),
)
