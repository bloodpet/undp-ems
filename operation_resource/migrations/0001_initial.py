# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EntityCategory'
        db.create_table('operation_resource_entitycategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('operation_resource', ['EntityCategory'])

        # Adding model 'EntityType'
        db.create_table('operation_resource_entitytype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.EntityCategory'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('operation_resource', ['EntityType'])

        # Adding model 'Agency'
        db.create_table('operation_resource_agency', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(unique=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('abbr', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('operation_resource', ['Agency'])

        # Adding model 'Entity'
        db.create_table('operation_resource_entity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('agency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.Agency'], null=True, blank=True)),
            ('number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(unique=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('abbr', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.EntityCategory'], null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.EntityType'], null=True, blank=True)),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.District'], null=True, blank=True)),
            ('county', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.County'], null=True, blank=True)),
            ('locality', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.Locality'], null=True, blank=True)),
        ))
        db.send_create_signal('operation_resource', ['Entity'])

        # Adding model 'VehicleCategory'
        db.create_table('operation_resource_vehiclecategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('operation_resource', ['VehicleCategory'])

        # Adding model 'VehicleType'
        db.create_table('operation_resource_vehicletype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.VehicleCategory'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('operation_resource', ['VehicleType'])

        # Adding model 'VehicleCode'
        db.create_table('operation_resource_vehiclecode', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.VehicleType'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('abbr', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('operation_resource', ['VehicleCode'])

        # Adding model 'Vehicle'
        db.create_table('operation_resource_vehicle', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.Entity'], null=True, blank=True)),
            ('code', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.VehicleCode'], null=True, blank=True)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('garrison', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.District'], null=True, blank=True)),
            ('county', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.County'], null=True, blank=True)),
            ('locality', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.Locality'], null=True, blank=True)),
            ('start_ts', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('end_ts', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('is_operational', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_available', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mark', self.gf('django.db.models.fields.CharField')(max_length=256, null=True)),
            ('model', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('license', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('operation_resource', ['Vehicle'])

        # Adding model 'Inoperationality'
        db.create_table('operation_resource_inoperationality', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vehicle', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.Vehicle'])),
        ))
        db.send_create_signal('operation_resource', ['Inoperationality'])


    def backwards(self, orm):
        # Deleting model 'EntityCategory'
        db.delete_table('operation_resource_entitycategory')

        # Deleting model 'EntityType'
        db.delete_table('operation_resource_entitytype')

        # Deleting model 'Agency'
        db.delete_table('operation_resource_agency')

        # Deleting model 'Entity'
        db.delete_table('operation_resource_entity')

        # Deleting model 'VehicleCategory'
        db.delete_table('operation_resource_vehiclecategory')

        # Deleting model 'VehicleType'
        db.delete_table('operation_resource_vehicletype')

        # Deleting model 'VehicleCode'
        db.delete_table('operation_resource_vehiclecode')

        # Deleting model 'Vehicle'
        db.delete_table('operation_resource_vehicle')

        # Deleting model 'Inoperationality'
        db.delete_table('operation_resource_inoperationality')


    models = {
        'location.county': {
            'Meta': {'object_name': 'County'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'location.district': {
            'Meta': {'object_name': 'District'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'location.locality': {
            'Meta': {'object_name': 'Locality'},
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'operation_resource.agency': {
            'Meta': {'object_name': 'Agency'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True'})
        },
        'operation_resource.entity': {
            'Meta': {'object_name': 'Entity'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'agency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Agency']", 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.EntityCategory']", 'null': 'True', 'blank': 'True'}),
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']", 'null': 'True', 'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Locality']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.EntityType']", 'null': 'True', 'blank': 'True'})
        },
        'operation_resource.entitycategory': {
            'Meta': {'object_name': 'EntityCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'operation_resource.entitytype': {
            'Meta': {'object_name': 'EntityType'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.EntityCategory']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'operation_resource.inoperationality': {
            'Meta': {'object_name': 'Inoperationality'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vehicle': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Vehicle']"})
        },
        'operation_resource.vehicle': {
            'Meta': {'object_name': 'Vehicle'},
            'code': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.VehicleCode']", 'null': 'True', 'blank': 'True'}),
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']", 'null': 'True', 'blank': 'True'}),
            'end_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Entity']", 'null': 'True', 'blank': 'True'}),
            'garrison': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_operational': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'license': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'locality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Locality']", 'null': 'True', 'blank': 'True'}),
            'mark': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'start_ts': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'operation_resource.vehiclecategory': {
            'Meta': {'object_name': 'VehicleCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'operation_resource.vehiclecode': {
            'Meta': {'object_name': 'VehicleCode'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.VehicleType']", 'null': 'True', 'blank': 'True'})
        },
        'operation_resource.vehicletype': {
            'Meta': {'object_name': 'VehicleType'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.VehicleCategory']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['operation_resource']