# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Vehicle.mark'
        db.alter_column('operation_resource_vehicle', 'mark', self.gf('django.db.models.fields.CharField')(default='', max_length=256))

    def backwards(self, orm):

        # Changing field 'Vehicle.mark'
        db.alter_column('operation_resource_vehicle', 'mark', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

    models = {
        'location.county': {
            'Meta': {'object_name': 'County'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'location.district': {
            'Meta': {'object_name': 'District'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'location.locality': {
            'Meta': {'object_name': 'Locality'},
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'operation_resource.agency': {
            'Meta': {'object_name': 'Agency'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True'})
        },
        'operation_resource.entity': {
            'Meta': {'object_name': 'Entity'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'agency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Agency']", 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.EntityCategory']", 'null': 'True', 'blank': 'True'}),
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']", 'null': 'True', 'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Locality']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.EntityType']", 'null': 'True', 'blank': 'True'})
        },
        'operation_resource.entitycategory': {
            'Meta': {'object_name': 'EntityCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'operation_resource.entitytype': {
            'Meta': {'object_name': 'EntityType'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.EntityCategory']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'operation_resource.inoperationality': {
            'Meta': {'object_name': 'Inoperationality'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vehicle': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Vehicle']"})
        },
        'operation_resource.vehicle': {
            'Meta': {'object_name': 'Vehicle'},
            'code': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.VehicleCode']", 'null': 'True', 'blank': 'True'}),
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']", 'null': 'True', 'blank': 'True'}),
            'end_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Entity']", 'null': 'True', 'blank': 'True'}),
            'garrison': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_operational': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'license': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'locality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Locality']", 'null': 'True', 'blank': 'True'}),
            'mark': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'start_ts': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'operation_resource.vehiclecategory': {
            'Meta': {'object_name': 'VehicleCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'operation_resource.vehiclecode': {
            'Meta': {'object_name': 'VehicleCode'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.VehicleType']", 'null': 'True', 'blank': 'True'})
        },
        'operation_resource.vehicletype': {
            'Meta': {'object_name': 'VehicleType'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.VehicleCategory']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['operation_resource']