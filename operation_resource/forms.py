__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@bloodpet.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


import logging
from django import forms
from django.utils.translation import ugettext_lazy as _lazy, ugettext as _
from operation_resource.models import (
    VehicleCategory, VehicleType, VehicleCode, Vehicle
)

log = logging.getLogger('xaasy')


class VehicleChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return u'%s' % obj


class VehicleSearchForm(forms.Form):
    category = VehicleChoiceField(
        queryset=VehicleCategory.objects.all(),
        empty_label=_(u'All'),
        required=False,
    )
    type = VehicleChoiceField(
        queryset=VehicleType.objects.all(),
        empty_label=_(u'All'),
        required=False,
    )
    code = VehicleChoiceField(
        queryset=VehicleCode.objects.all(),
        empty_label=_(u'All'),
        required=False,
    )

    class Meta:
        model = Vehicle

    def get_filters(self):
        log.error('%s %s' % (self.__class__.__name__, self.cleaned_data))
        filters = {}
        data = self.cleaned_data
        if data['code'] is not None:
            filters['code'] = data['code']
        elif data['type'] is not None:
            filters['code__type'] = data['type']
        elif data['category'] is not None:
            filters['code__type__category'] = data['category']
        return filters

    def update_fields(self):
        log.error('%s %s' % (self.__class__.__name__, self.cleaned_data))
        data = self.cleaned_data
        if data['category'] is not None:
            self.fields['type'].queryset = VehicleType.objects.filter(category=data['category'])
            self.fields['code'].queryset = VehicleCode.objects.filter(type__category=data['category'])
        if data['type'] is not None:
            self.fields['type'].queryset = VehicleType.objects.filter(category=data['type'])


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
