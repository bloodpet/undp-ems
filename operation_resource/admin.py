__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline
from operation_resource.models import (
    EntityCategory, EntityType, Agency, Entity, 
    VehicleCategory, VehicleType, VehicleCode, Vehicle, 
)


class EntityCategoryInline(admin.TabularInline):
    model = EntityCategory


class EntityTypeInline(admin.TabularInline):
    model = EntityType


class AgencyInline(admin.TabularInline):
    model = Agency


class EntityInline(admin.TabularInline):
    model = Entity


class VehicleCategoryInline(admin.TabularInline):
    model = VehicleCategory


class VehicleTypeInline(admin.TabularInline):
    model = VehicleType


class VehicleCodeInline(admin.TabularInline):
    model = VehicleCode


class VehicleInline(admin.TabularInline):
    model = Vehicle


class EntityCategoryAdmin(admin.ModelAdmin):
    inlines = []


class EntityTypeAdmin(admin.ModelAdmin):
    inlines = [ ]


class AgencyAdmin(admin.ModelAdmin):
    inlines = []


class EntityAdmin(admin.ModelAdmin):
    inlines = [ ]
    fields = (
        'agency',
        'number',
        'name',
        'abbr',
        'district',
        'county',
        'locality',
    )


class VehicleCategoryAdmin(admin.ModelAdmin):
    inlines = []


class VehicleTypeAdmin(admin.ModelAdmin):
    inlines = [VehicleCodeInline, ]


class VehicleCodeAdmin(admin.ModelAdmin):
    inlines = []


class VehicleAdmin(admin.ModelAdmin):
    inlines = [ ]
    fields = (
        'entity',
        'code',
        'number',
        'garrison',
        'description',
        'is_operational',
        'is_available',
        'mark',
        'model',
        'license',
    )


admin.site.register(EntityCategory, EntityCategoryAdmin)
admin.site.register(EntityType, EntityTypeAdmin)
admin.site.register(Agency, AgencyAdmin)
admin.site.register(Entity, EntityAdmin)
admin.site.register(VehicleCategory, VehicleCategoryAdmin)
admin.site.register(VehicleType, VehicleTypeAdmin)
admin.site.register(VehicleCode, VehicleCodeAdmin)
admin.site.register(Vehicle, VehicleAdmin)
