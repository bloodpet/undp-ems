__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from emergency_management import models
from django.utils.encoding import smart_unicode
from django.utils.translation import ugettext_lazy as _lazy, ugettext as _
from location.models import District, County, Locality


class EntityCategory(models.NamedModel):
    name = models.CharField(_(u'Name'), max_length=256)

    class Meta:
        verbose_name = _(u'Category')
        verbose_name_plural = _(u'Categories')
        verbose_name = _lazy(u'Category')
        verbose_name_plural = _lazy(u'Categories')

class EntityType(models.NamedModel):
    category = models.ForeignKey(EntityCategory, verbose_name=_(u'Category'), blank=True, null=True)
    name = models.CharField(_(u'Name'), max_length=256)

    class Meta:
        verbose_name = _(u'Type')
        verbose_name_plural = _(u'Types')
        verbose_name = _lazy(u'Type')
        verbose_name_plural = _lazy(u'Types')


class Agency(models.AbbreviatedModel):
    number = models.PositiveSmallIntegerField(_(u'Code'), unique=True)
    name = models.CharField(_(u'Name'), max_length=256)
    abbr = models.CharField(_(u'Abbreviated Name'), max_length=256)

    class Meta:
        verbose_name = _('Entity')
        verbose_name_plural = _('Entities')
        verbose_name = _lazy('Entity')
        verbose_name_plural = _lazy('Entities')

    def __unicode__(self):
        return u'%0*d %s' % (2, self.number, self.abbr)


class Entity(models.AbbreviatedModel):
    agency = models.ForeignKey(Agency, verbose_name=_(u'Entity'), blank=True, null=True)
    number = models.PositiveSmallIntegerField(_(u'Code'), unique=True)
    name = models.CharField(_(u'Name'), max_length=256)
    abbr = models.CharField(_(u'Abbreviated Name'), max_length=256)
    category = models.ForeignKey(EntityCategory, verbose_name=_(u'Category'), blank=True, null=True)
    type = models.ForeignKey(EntityType, verbose_name=_(u'Type'), blank=True, null=True)
    # Location Fields
    district = models.ForeignKey(District, verbose_name=_(u'District'), blank=True, null=True)
    county = models.ForeignKey(County, verbose_name=_(u'Subdistrict'), blank=True, null=True)
    locality = models.ForeignKey(Locality, verbose_name=_(u'Suco'), blank=True, null=True)

    class Meta:
        verbose_name = _('Unit')
        verbose_name_plural = _('Units')
        verbose_name = _lazy('Unit')
        verbose_name_plural = _lazy('Units')

    def __unicode__(self):
        return u'%0*d%0*d %s' % (2, self.agency.number, 4, self.number, self.abbr)


# Vehicle related models


class VehicleCategory(models.NamedModel):
    name = models.CharField(_(u'Name'), max_length=256)

    class Meta:
        verbose_name = _(u'Vehicle Category')
        verbose_name_plural = _(u'Vehicle Categories')
        verbose_name = _lazy(u'Vehicle Category')
        verbose_name_plural = _lazy(u'Vehicle Categories')


class VehicleType(models.NamedModel):
    category = models.ForeignKey(VehicleCategory, verbose_name=_(u'Category'), blank=True, null=True)
    name = models.CharField(_(u'Name'), max_length=256)

    class Meta:
        verbose_name = _(u'Vehicle Type')
        verbose_name_plural = _(u'Vehicle Types')
        verbose_name = _lazy(u'Vehicle Type')
        verbose_name_plural = _lazy(u'Vehicle Types')


class VehicleCode(models.AbbreviatedModel):
    type = models.ForeignKey(VehicleType, verbose_name=_(u'Type'), blank=True, null=True)
    name = models.CharField(_(u'Name'), max_length=256)
    abbr = models.CharField(_(u'Abbreviated Name'), max_length=256)

    class Meta:
        verbose_name = _(u'Operational Code')
        verbose_name_plural = _(u'Operational Codes')
        verbose_name = _lazy(u'Operational Code')
        verbose_name_plural = _lazy(u'Operational Codes')


class Vehicle(models.Model):
    entity = models.ForeignKey(Entity, verbose_name=_(u'Unit'), blank=True, null=True)
    code = models.ForeignKey(VehicleCode, verbose_name=_(u'Operational Code'), blank=True, null=True)
    number = models.PositiveIntegerField(_(u'Number'))
    garrison = models.PositiveIntegerField(_(u'Garrison'), default=1)
    # Location Fields
    district = models.ForeignKey(District, verbose_name=_(u'District'), blank=True, null=True)
    county = models.ForeignKey(County, verbose_name=_(u'Subdistrict'), blank=True, null=True)
    locality = models.ForeignKey(Locality, verbose_name=_(u'Suco'), blank=True, null=True)
    start_ts = models.DateTimeField(
        _(u'Date Entered Service'), auto_now_add=True, editable=False)
    end_ts = models.DateTimeField(
        _(u'Date Out of Service'), editable=False, null=True)
    description = models.TextField(_(u'Observation'), blank=True)
    is_operational = models.BooleanField(verbose_name=_(u'Operational'), default=True)
    is_available = models.BooleanField(verbose_name=_(u'Available'), default=False)
    # Chassis information
    mark = models.CharField(_(u'Mark'), max_length=256, blank=True)
    model = models.CharField(_(u'Model'), max_length=256, blank=True)
    license = models.CharField(_(u'License'), max_length=256, blank=True)

    class Meta:
        verbose_name = _('Vehicle')
        verbose_name_plural = _('Vehicles')
        verbose_name = _lazy('Vehicle')
        verbose_name_plural = _lazy('Vehicles')

    def __unicode__(self):
        return u'%s %s %0*d' % (self.locality, self.code, 2, self.number)


class Inoperationality(models.Model):
    vehicle = models.ForeignKey(
        Vehicle, verbose_name=_(u'Vehicle'), editable=True)

    class Meta:
        verbose_name = _('Date of Inoperationality')
        verbose_name_plural = _('Date of Inoperationalities')
        verbose_name = _lazy('Date of Inoperationality')
        verbose_name_plural = _lazy('Date of Inoperationalities')


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
