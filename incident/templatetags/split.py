# From http://stackoverflow.com/questions/6481788/format-of-timesince-filter
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

def split(value, delimiter=',', element=0):
    return value.split(delimiter)[element]

@register.filter
@stringfilter
def get_first(value, delimiter=','):
    return split(value, delimiter=delimiter, element=0)
get_first.is_safe = True


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
