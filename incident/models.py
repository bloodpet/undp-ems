__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from emergency_management import models
from django.utils.translation import ugettext_lazy as _lazy, ugettext as _
from django.contrib.auth.models import User
from django.utils.encoding import smart_unicode
from location.models import District, County, Locality
from operation_resource.models import (
    Entity, Vehicle
)


class Foreigners(object):

    def get_foreign_key(self, key, pk):
        foreign_keys = dict(
            alert=Alert,
            occurrence=Occurrence,
            nature=Nature,
            source=Source,
            source_type = SourceType,
            status=Status,
            importance=Importance,
            entity=Entity,
            district=District,
            county=County,
            locality=Locality,
        )
        return foreign_keys[key].objects.get(pk=pk)


class Importance(models.NamedModel):
    name = models.CharField(_(u'Name'), max_length=64)
    number = models.PositiveIntegerField(_(u'Number'), unique=True)

    class Meta:
        verbose_name = _(u'Importance')
        verbose_name_plural = _(u'Importances')
        verbose_name = _lazy(u'Importance')
        verbose_name_plural = _lazy(u'Importances')


class Status(models.NamedModel):
    name = models.CharField(_(u'Name'), max_length=256)

    class Meta:
        verbose_name = _(u'Status')
        verbose_name_plural = _(u'statuses')
        verbose_name = _lazy(u'Status')
        verbose_name_plural = _lazy(u'statuses')


class NatureFamily(models.Model):
    name = models.CharField(_(u'Name'), max_length=256)
    abbr = models.CharField(_(u'Abbreviation'), max_length=16)
    number = models.PositiveIntegerField(
        _(u'Number'), serialize=True,
        auto_created=True,
        unique_for_year='start_ts')

    class Meta:
        verbose_name = _(u'nature family')
        verbose_name_plural = _(u'nature families')
        verbose_name = _lazy(u'nature family')
        verbose_name_plural = _lazy(u'nature families')

    def __unicode__(self):
        return self.name


class NatureType(models.Model):
    nature_family = models.ForeignKey(NatureFamily, verbose_name=_(u'Family'))
    name = models.CharField(_(u'Name'), max_length=256)
    abbr = models.CharField(_(u'Abbreviation'), max_length=16)
    number = models.PositiveIntegerField(
        _(u'Number'), serialize=True,
        auto_created=True,
        unique_for_year='start_ts')

    class Meta:
        verbose_name = _(u'nature subfamily')
        verbose_name_plural = _(u'nature subfamilies')
        verbose_name = _lazy(u'nature subfamily')
        verbose_name_plural = _lazy(u'nature subfamilies')

    def __unicode__(self):
        return self.name


class Nature(models.Model):
    nature_type = models.ForeignKey(NatureType, verbose_name=_(u'Type'))
    name = models.CharField(_(u'Name'), max_length=256, blank=True)
    number = models.PositiveIntegerField(
        _(u'Number'), serialize=True,
        auto_created=True, default=0,
        unique_for_year='start_ts')

    class Meta:
        verbose_name = _(u'Nature')
        verbose_name_plural = _(u'Natures')
        verbose_name = _lazy(u'Nature')
        verbose_name_plural = _lazy(u'Natures')

    def __unicode__(self):
        txt = u'%s %s / %s' % ((
            ((1000 * self.nature_type.nature_family.number) +\
             (100 * self.nature_type.number) + self.number)),
            smart_unicode(self.nature_type.nature_family.abbr),
            smart_unicode(self.nature_type.abbr),
        )
        if self.name:
            txt += u' / ' + smart_unicode(self.name)
        return txt


class Source(models.NamedModel):
    name = models.CharField(_(u'Name'), max_length=256)

    class Meta:
        verbose_name = _(u'Source')
        verbose_name_plural = _(u'Sources')
        verbose_name = _lazy(u'Source')
        verbose_name_plural = _lazy(u'Sources')


class SourceType(models.NamedModel):
    name = models.CharField(_(u'Name'), max_length=256)

    class Meta:
        verbose_name = u'%s %s/%s' % (_(u'Source'), _('Entity'), _('Citizen'))
        verbose_name_plural = u'%s %s/%s' % (_(u'Source'), _('Entities'), _('Citizens'))
        verbose_name = u'%s %s/%s' % (_lazy(u'Source'), _lazy('Entity'), _lazy('Citizen'))
        verbose_name_plural = u'%s %s/%s' % (_lazy(u'Source'), _lazy('Entities'), _lazy('Citizens'))


class Alert(models.SerializedModel):
    user = models.ForeignKey(User, editable=False)
    occurrence = models.ForeignKey('Occurrence', blank=True, null=True, editable=False)
    number = models.PositiveIntegerField(
        _(u'Number'), serialize=True, default=0,
        auto_created=True, editable=False,
        unique_for_year='start_ts')
    start_ts = models.DateTimeField(
        _(u'Time'), auto_now_add=True, editable=False)
    end_ts = models.DateTimeField(
        _(u'Time Ended'), editable=False, null=True)
    nature = models.ForeignKey(Nature, verbose_name=_('Nature'), default=1)
    description = models.TextField(_(u'Description'), blank=True)
    source = models.ForeignKey(Source, verbose_name=_(u'Source'), blank=True, null=True)
    source_type = models.ForeignKey(
        SourceType,
        verbose_name=u'%s %s/%s' % (_(u'Source'), _('Entity'), _('Citizen')),
        blank=True, null=True)
    name = models.CharField(_(u'Name'), max_length=256, blank=True)
    phone = models.CharField(_(u'Phone'), max_length=32, blank=True)
    # Location Fields
    district = models.ForeignKey(District, verbose_name=_(u'District'), blank=True, null=True)
    county = models.ForeignKey(County, verbose_name=_(u'Subdistrict'), blank=True, null=True)
    locality = models.ForeignKey(Locality, verbose_name=_(u'Suco'), blank=True, null=True)
    local = models.CharField(_(u'Aldea'), max_length=256, blank=True)
    point_of_reference = models.TextField(_(u'Point of Reference'), blank=True)
    # Geolocation Fields
    latitude = models.FloatField(_(u'Latitude'), blank=True, null=True)
    longitude = models.FloatField(_(u'Longitude'), blank=True, null=True)
    # False alarm
    is_false = models.BooleanField(verbose_name=_(u'False Alert'), default=False)

    @models.permalink
    def get_absolute_url(self):
        return ('alert_edit', [self.pk, ])


class Occurrence(models.SerializedModel):
    user = models.ForeignKey(User, editable=False)
    occurrence = models.ForeignKey(
        'self', blank=True, null=True, editable=False, related_name='associated'
    )
    # Use this for symmetric relationship.
    #occurrence = models.ManyToManyField(
        #'self', blank=True, null=True, editable=False,
    #)
    number = models.PositiveIntegerField(
        _(u'Number'), serialize=True, default=0,
        auto_created=True, editable=False,
        unique_for_year='start_ts')
    start_ts = models.DateTimeField(
        _(u'Time'), auto_now_add=True, editable=False)
    end_ts = models.DateTimeField(
        _(u'Time Ended'), editable=False, null=True)
    importance = models.ForeignKey(Importance, verbose_name=_(u'Importance'), default=1)
    status = models.ForeignKey(Status, verbose_name=_(u'Status'), default=1)
    nature = models.ForeignKey(Nature, default=1)
    description = models.TextField(_(u'Description'), blank=True)
    entity = models.ForeignKey(Entity, verbose_name=_(u'Unit'), blank=True, null=True)
    # Location Fields
    district = models.ForeignKey(District, verbose_name=_(u'District'), blank=True, null=True)
    county = models.ForeignKey(County, verbose_name=_(u'Subdistrict'), blank=True, null=True)
    locality = models.ForeignKey(Locality, verbose_name=_(u'Suco'), blank=True, null=True)
    local = models.CharField(_(u'Aldea'), max_length=256, blank=True)
    point_of_reference = models.TextField(_(u'Point of Reference'), blank=True)
    # Geolocation Fields
    latitude = models.FloatField(_(u'Latitude'), blank=True, null=True)
    longitude = models.FloatField(_(u'Longitude'), blank=True, null=True)

    @models.permalink
    def get_absolute_url(self):
        return ('occurrence_edit', [self.pk, ])

    def save(self, *args, **kwargs):
        from datetime import datetime
        if self.status.pk in (5, 7):
            self.end_ts = datetime.now()
        return super(Occurrence, self).save(*args, **kwargs)


class ResourceType(models.Model):
    name = models.CharField(_(u'Name'), max_length=256)

    def __unicode__(self):
        return self.name


class Resource(models.Model):
    name = models.CharField(_(u'Name'), max_length=256)
    resource_type = models.ForeignKey(
        ResourceType, verbose_name=_(u'Type'), editable=True)

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('resource_edit', [self.pk, ])


class DispatchStatus(models.NamedModel):
    name = models.CharField(_(u'Name'), max_length=256)

    class Meta:
        verbose_name = _(u'Dispatch Status')
        verbose_name_plural = _(u'Dispatch Statuses')
        verbose_name = _lazy(u'Dispatch Status')
        verbose_name_plural = _lazy(u'Dispatch Statuses')


class Dispatch(models.Model):
    user = models.ForeignKey(User, editable=False)
    occurrence = models.ForeignKey(
        Occurrence, verbose_name=_(u'Occurrence'), editable=False)
    entity = models.ForeignKey(
        Entity, verbose_name=_(u'Unit'), editable=True,
        blank=True, null=True,
    )
    vehicle = models.ForeignKey(
        Vehicle, verbose_name=_(u'Vehicle'), editable=True,
        blank=True, null=True,
    )
    start_ts = models.DateTimeField(
        _(u'Dispatch'), auto_now_add=True, editable=True)
    exit_hq_ts = models.DateTimeField(
        _(u'Exit from Entity'), editable=True, blank=True, null=True)
    arrive_to_ts = models.DateTimeField(
        _(u'Arrive to Local'), editable=True, blank=True, null=True)
    exit_to_ts = models.DateTimeField(
        _(u'Exit Local'), editable=True, blank=True, null=True)
    arrive_hq_ts = models.DateTimeField(
        _(u'Arrive to Entity'), editable=True, blank=True, null=True)
    status = models.ForeignKey(
        DispatchStatus, verbose_name=_(u'Status'), default=1,
        blank=True, null=True,
    )
    observations = models.TextField(_(u'Observations'), blank=True)

    def __unicode__(self):
        return '%s' % self.vehicle or ''

    def delete(self):
        self.vehicle.is_available = True
        self.vehicle.save()
        return super(Dispatch, self).delete()

    def release(self):
        if self.exit_to_ts is not None:
            self.arrive_hq_ts = self.exit_to_ts
        elif self.arrive_to_ts is not None:
            self.exit_to_ts = self.arrive_to_ts
            self.arrive_hq_ts = self.exit_to_ts
        elif self.exit_hq_ts is not None:
            self.arrive_to_ts = self.exit_hq_ts
            self.exit_to_ts = self.arrive_to_ts
            self.arrive_hq_ts = self.exit_to_ts
        else:
            self.exit_hq_ts = self.start_ts
            self.arrive_to_ts = self.exit_hq_ts
            self.exit_to_ts = self.arrive_to_ts
            self.arrive_hq_ts = self.exit_to_ts
        self.status = DispatchStatus.objects.get(pk=5)
        self.save()
        if self.occurrence.dispatch_set.filter(status__pk__lt=5).count() == 0:
            self.occurrence.status = Status.objects.get(pk=7)
            self.occurrence.save()
        elif self.occurrence.dispatch_set.filter(status__pk=5).count() == 1:
            self.occurrence.status = Status.objects.get(pk=5)
            self.occurrence.save()
        self.vehicle.is_available = True
        self.vehicle.save()
        return self

    def save(self, *args, **kwargs):
        if self.arrive_hq_ts is not None:
            self.status = DispatchStatus.objects.get(pk=5)
            if self.occurrence.dispatch_set.filter(status__pk__lt=5).count() == 0:
                # When no vehicle is left, mark occurrence as closed
                self.occurrence.status = Status.objects.get(pk=7)
                self.occurrence.save()
            elif self.occurrence.dispatch_set.filter(status__pk=5).count() == 1:
                # When the first vehicle goes back to the hq,
                # mark it as concluded
                self.occurrence.status = Status.objects.get(pk=5)
                self.occurrence.save()
            self.vehicle.is_available = True
            self.vehicle.save()
        elif self.exit_to_ts is not None:
            self.status = DispatchStatus.objects.get(pk=4)
        elif self.arrive_to_ts is not None:
            self.status = DispatchStatus.objects.get(pk=3)
        elif self.exit_hq_ts is not None:
            self.status = DispatchStatus.objects.get(pk=2)
        else:
            self.status = DispatchStatus.objects.get(pk=1)
        return super(Dispatch, self).save(*args, **kwargs)

    @models.permalink
    def get_absolute_url(self):
        return ('dispatch_edit', [self.occurrence.pk, self.pk, ])


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
