from csv import reader
from django.core.management.base import LabelCommand
from incident.models import District, County, Locality


class Command(LabelCommand):
    args = '<File1 File2 ...>'
    help = 'Create locations from one or more csv files'

    def create_location(self, row):
        district, new = District.objects.get_or_create(
            number=int(row[0]),
            name=row[1],
        )
        if new:
            district.save()
        county, new = County.objects.get_or_create(
            district=district,
            number=int(row[2]),
            name=row[3],
        )
        if new:
            county.save()
        locality, new = Locality.objects.get_or_create(
            county=county,
            number=int(row[4]),
            name=row[5],
        )
        if new:
            locality.save()
        return locality

    def handle(self, *labels, **options):
        for model in District, County, Locality:
            for entry in model.objects.all():
                entry.delete()
        return super(Command, self).handle(*labels, **options)

    def handle_label(self, label, **options):
        with open(label) as fd:
            rows = reader(fd)
            rows.next()
            rows.next()
            for row in rows:
                print row
                location = self.create_location(row)
                print location
            fd.close()
            return
        raise Warning('Cannot open file %s for reading.' % label)
