from csv import reader
from django.core.management.base import LabelCommand
from incident.models import NatureFamily, NatureType, Nature


class Command(LabelCommand):
    args = '<File1 File2 ...>'
    help = 'Create natures from one or more csv files'

    def create_nature(self, row):
        number = int(row[0])
        family, new = NatureFamily.objects.get_or_create(
            number=int(number/1000),
            abbr=row[1],
            name=row[2],
        )
        if new:
            family.save()
        specie, new = NatureType.objects.get_or_create(
            nature_family=family,
            number=int((number/100) % 10),
            abbr=row[4],
            name=row[5],
        )
        if new:
            specie.save()
        nature = Nature.objects.create(
            nature_type=specie,
            number=int(number % 100),
            #abbr=row[7],
            name=row[8],
        )
        nature.save()
        return nature

    def handle(self, *labels, **options):
        for model in NatureFamily, NatureType, Nature:
            for entry in model.objects.all():
                entry.delete()
        return super(Command, self).handle(*labels, **options)

    def handle_label(self, label, **options):
        with open(label) as fd:
            rows = reader(fd)
            rows.next()
            rows.next()
            for row in rows:
                print row
                nature = self.create_nature(row)
                print nature
            fd.close()
            return
        raise Warning('Cannot open file %s for reading.' % label)
