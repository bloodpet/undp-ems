__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@bloodpet.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


import logging
from datetime import datetime, timedelta
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.shortcuts import urlresolvers
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _lazy, ugettext as _
from django.views.generic import *
from emergency_management.utils import add_entry, edit_entry, del_entry
from operation_resource.forms import VehicleSearchForm
from operation_resource.models import (
    Entity, VehicleCategory, VehicleType, VehicleCode, Vehicle
)
from incident.models import Alert, Occurrence, Resource, Foreigners, Dispatch, Status


MAX_STATUS = 3
log = logging.getLogger('xaasy')


class OwnedCreateView(CreateView):

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        add_entry(self.request, self.object)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context_data = super(OwnedCreateView, self).get_context_data(**kwargs)
        context_data['is_new'] = True
        return context_data


class MutableListView(ListView):

    def post(self, request, pk=None, *args, **kwargs):
        if pk is not None:
            self.occurrence = self.object = Occurrence.objects.get(pk=pk)
        action = request.POST['action']
        try:
            func = getattr(self, action)
        except Exception, e:
            raise Exception(e)
            return HttpResponseBadRequest('Nothing to do for "%s"' % action)
        pks = request.POST.getlist('select')
        return func(pks)

    def remove(self, pks):
        entries = self.model.objects.filter(pk__in=pks)
        for entry in entries:
            del_entry(self.request, entry)
        entries.delete()
        return HttpResponseRedirect(self.request.path)

    def associate_occurrence(self, pks):
        entries = self.model.objects.filter(pk__in=pks)
        self.object = occurrence = Occurrence.objects.get(pk=self.request.POST['occurrence'])
        for entry in entries:
            entry.occurrence = occurrence
            entry.save()
            edit_entry(self.request, entry)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context_data = super(MutableListView, self).get_context_data(**kwargs)
        last_week = datetime.now() - timedelta(7)
        context_data['occurrence_list'] = Occurrence.objects.filter(
            start_ts__gte=last_week,
            status__pk__lte=MAX_STATUS)
        return context_data


class ListAssociateView(MutableListView):
    template_name = 'incident/associate_occurrence.html'

    def get(self, request):
        self.pks = request.GET.getlist('select')
        return super(MutableListView, self).get(request)

    def get_context_data(self, **kwargs):
        context_data = super(MutableListView, self).get_context_data(**kwargs)
        last_week = datetime.now() - timedelta(7)
        context_data['occurrence_list'] = Occurrence.objects.filter(
            start_ts__gte=last_week,
            status__pk__lte=MAX_STATUS)
        context_data['new_window'] = True
        return context_data

    def get_queryset(self):
        queryset = self.model.objects.filter(pk__in=self.pks)
        return queryset

    def create_occurrence(self):
        return HttpResponseRedirect(
            urlresolvers.reverse('create_occurrence'))


class AutoSaveUpdateView(UpdateView, Foreigners):

    def get(self, request, **kw):
        self.object = self.model.objects.get(**kw)
        log.error('Autosave %s' % self.object)
        for k,v in request.GET.iteritems():
            k, v = self.get_kv(k, v)
            try:
                getattr(self.object, k)
            except AttributeError, e:
                log.error(e)
            else:
                try:
                    setattr(self.object, k, v)
                except ValueError:
                    try:
                        v = self.get_foreign_key(k, v)
                    except ValueError:
                        v = None
                    setattr(self.object, k, v)
                self.object.save()
                log.debug('Saved %s' % self.object)
                edit_entry(request, self.object)
        return super(AutoSaveUpdateView, self).get(request, **kw)

    def get_kv(self, k, v):
        if k == 'date':
            tz = timezone.get_current_timezone()
            k = 'start_ts'
            old = self.object.start_ts
            new = datetime.strptime(v, '%Y-%m-%d')
            v = old.replace(new.year, new.month, new.day)#, tzinfo=tz)
        elif k == 'time':
            tz = timezone.get_current_timezone()
            k = 'start_ts'
            old = self.object.start_ts
            new = datetime.strptime(v, '%H:%M')
            v = old.replace(hour=new.hour, minute=new.minute)#, tzinfo=tz)
        elif k.endswith('_date'):
            tz = timezone.get_current_timezone()
            k = k.replace('_date', '_ts')
            old = getattr(self.object, k)
            if old is None:
                old = self.object.start_ts
            new = datetime.strptime(v, '%Y-%m-%d')
            v = old.replace(new.year, new.month, new.day)#, tzinfo=tz)
        elif k.endswith('_time'):
            tz = timezone.get_current_timezone()
            k = k.replace('_time', '_ts')
            old = getattr(self.object, k)
            if old is None:
                old = self.object.start_ts
            new = datetime.strptime(v, '%H:%M')
            v = old.replace(hour=new.hour, minute=new.minute)#, tzinfo=tz)
        return k, v


class AssociateView(UpdateView):
    template_name = 'incident/associate_occurrence.html'

    def get_context_data(self, **kwargs):
        context_data = super(AssociateView, self).get_context_data(**kwargs)
        last_week = datetime.now() - timedelta(7)
        context_data['occurrence_list'] = Occurrence.objects.filter(
            start_ts__gte=last_week,
            status__pk__lte=MAX_STATUS)
        return context_data

    def post(self, request, pk):
        self.object = self.model.objects.get(pk=pk)
        occurrence_pk = request.POST['occurrence']
        occurrence = Occurrence.objects.get(pk=occurrence_pk)
        log.debug('Associating %s with %s' % (self.object, occurrence))
        self.object.occurrence = occurrence
        self.object.save()
        edit_entry(request, self.object)
        return HttpResponseRedirect(self.get_success_url())


class GotoPage(View):

    def get(self, request):
        page = request.GET['page']
        pk = request.GET['pk']
        return HttpResponseRedirect(urlresolvers.reverse(page, args=[pk]))


class CreateAssociated(View):

    def get(self, request):
        action = request.GET['action']
        if action == 'alert':
            model = Alert
        elif action == 'occurrence':
            model = Occurrence
        else:
            return HttpResponseBadRequest('Nothing to do for "%s"' % action)
        pks = request.GET.getlist('select')
        entries = model.objects.filter(pk__in=pks).order_by('start_ts')
        entry = entries[0]
        occurrence = Occurrence(user=request.user)
        occurrence.save()
        for e in entries:
            e.occurrence = occurrence
            e.save()
            edit_entry(request, e)
        for k,v in dict(
            start_ts=entry.start_ts,
            nature=entry.nature,
            description=entry.description,
            district=entry.district,
            county=entry.county,
            locality=entry.locality,
            local=entry.local,
            point_of_reference=entry.point_of_reference,
            longitude=entry.longitude,
            latitude=entry.latitude,
        ).iteritems():
            setattr(occurrence, k, v)
        occurrence.save()
        add_entry(request, occurrence)
        return HttpResponseRedirect(urlresolvers.reverse(
                'occurrence_edit', args=[occurrence.pk]))


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context_data = super(self.__class__, self).get_context_data(**kwargs)
        last_week = datetime.now() - timedelta(7)
        context_data['alert_list'] = Alert.objects.filter(
            is_false=False,
            start_ts__gte=last_week,
            occurrence=None)
        context_data['occurrence_list'] = Occurrence.objects.filter(
            start_ts__gte=last_week,
            status__pk__lte=MAX_STATUS)
        return context_data


class AlertListView(MutableListView):
    model = Alert
    template_object_name = 'alert'

    def get_queryset(self):
        queryset = self.model.objects.filter(
            is_false=False,
            occurrence=None,
        )
        return queryset

    def create_occurrence(self):
        return HttpResponseRedirect(urlresolvers.reverse('create_occurrence'))

    def mark_false(self, pks):
        entries = self.model.objects.filter(pk__in=pks)
        for entry in entries:
            entry.is_false = True
            entry.save()
            edit_entry(self.request, entry)
        return HttpResponseRedirect(self.request.path)


class AlertCreateView(OwnedCreateView):
    model = Alert

    def get_context_data(self, **kwargs):
        context_data = super(self.__class__, self).get_context_data(**kwargs)
        context_data['page_title'] = _(u'Create Alert')
        return context_data


class AlertDetailView(DetailView):
    model = Alert
    template_object_name = 'alert'


class AlertUpdateView(AutoSaveUpdateView):
    model = Alert

    def form_valid(self, form):
        from datetime import datetime
        self.object = form.save(commit=False)
        ts_date = self.request.POST['date']
        ts_time = self.request.POST['time']
        try:
            ts = datetime.strptime('%s %s' % (ts_date, ts_time), '%Y-%m-%d %H:%M')
        except Exception:
            pass
        else:
            self.object.start_ts = ts.replace(tzinfo=timezone.get_current_timezone())
        self.object.save()
        edit_entry(self.request, self.object)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context_data = super(self.__class__, self).get_context_data(**kwargs)
        context_data['page_title'] = u'%s %s' % (_(u'Alert'), self.object)
        return context_data


class AlertAssociateView(AssociateView):
    model = Alert

    def get_success_url(self):
        return urlresolvers.reverse(
            'occurrence_alerts', args=[self.object.occurrence.pk])


class AlertListAssociateView(ListAssociateView):
    model = Alert
    template_object_name = 'alert'

    def get_success_url(self):
        return urlresolvers.reverse(
            'occurrence_alerts', args=[self.object.pk])


class OccurrenceCreateView(OwnedCreateView):
    model = Occurrence

    def get_context_data(self, **kwargs):
        context_data = super(self.__class__, self).get_context_data(**kwargs)
        context_data['page_title'] = _(u'Create Occurrence')
        return context_data


class OccurrenceDetailView(DetailView):
    model = Occurrence
    template_object_name = 'occurrence'


class OccurrenceUpdateView(AutoSaveUpdateView):
    model = Occurrence

    def get_context_data(self, **kwargs):
        context_data = super(self.__class__, self).get_context_data(**kwargs)
        context_data['page_title'] = u'%s %s' % (_(u'Occurrence'), self.object)
        return context_data


class OccurrenceListView(MutableListView):
    model = Occurrence
    template_object_name = 'occurrence'
    queryset = Occurrence.objects.filter(status__pk__lte=7)


class OccurrenceAssociateView(AssociateView):
    model = Occurrence

    def get_success_url(self):
        return urlresolvers.reverse(
            'occurrence_occurrences', args=[self.object.occurrence.pk])


class OccurrenceAssociated(MutableListView):

    def disassociate(self, pks):
        entries = self.model.objects.filter(pk__in=pks)
        for entry in entries:
            entry.occurrence = None
            entry.save()
            edit_entry(self.request, entry)
        return HttpResponseRedirect(self.request.path)

    def get(self, request, pk):
        self.object = Occurrence.objects.get(pk=pk)
        return super(MutableListView, self).get(self, request, pk=pk)

    def get_context_data(self, **kwargs):
        context_data = super(MutableListView, self).get_context_data(**kwargs)
        context_data['object'] = self.object
        return context_data

    def get_queryset(self):
        queryset = self.model.objects.filter(occurrence=self.object)
        return queryset


class OccurrenceAlertsView(OccurrenceAssociated):
    model = Alert
    template_name = 'incident/occurrence_alerts.html'


class OccurrenceOccurrencesView(OccurrenceAssociated):
    model = Occurrence
    template_name = 'incident/occurrence_occurrences.html'


class OccurrenceListAssociateView(ListAssociateView):
    model = Occurrence
    template_object_name = 'occurrence'

    def get_success_url(self):
        return urlresolvers.reverse(
            'occurrence_occurrences', args=[self.object.pk])


class OccurrenceDispatchListView(OccurrenceAssociated):
    model = Dispatch

    def remove(self, pks):
        entries = self.model.objects.filter(pk__in=pks)
        for entry in entries:
            entry.vehicle.is_available = True
            entry.vehicle.save()
            del_entry(self.request, entry)
        entries.delete()
        if self.occurrence.status == Status.objects.get(pk=2) and \
           self.occurrence.dispatch_set.count() == 0:
            self.occurrence.status = Status.objects.get(pk=1)
            self.occurrence.save()
        return HttpResponseRedirect(self.request.path)

    def release(self, pks):
        entries = self.model.objects.filter(pk__in=pks)
        for entry in entries:
            entry.release()
            edit_entry(self.request, entry)
        return HttpResponseRedirect(self.request.path)


class DispatchEntityListView(OccurrenceAssociated):
    model = Vehicle
    template_name = 'incident/dispatch_choose_entity.html'

    def get(self, request, occurrence_pk, **kw):
        self.occurrence = Occurrence.objects.get(pk=occurrence_pk)
        self.entity_list = Entity.objects.all()
        return super(DispatchEntityListView, self).get(request, occurrence_pk, **kw)

    def get_entity(self):
        entity_pk = self.request.GET.get('entity', None)
        if entity_pk == '0':
            entity = 0
        elif entity_pk is None:
            entity = self.occurrence.entity
        else:
            entity = Entity.objects.get(pk=entity_pk)
        return entity

    def get_context_data(self, **kwargs):
        context_data = super(DispatchEntityListView, self).get_context_data(**kwargs)
        last_week = datetime.now() - timedelta(7)
        context_data['occurrence'] = self.occurrence
        context_data['entity_list'] = self.entity_list
        entity = self.get_entity()
        if entity == 0 or entity is None:
            context_data['chosen_entity'] = 0
        else:
            context_data['chosen_entity'] = entity.pk
        return context_data

    def get_queryset(self):
        filters = dict(
            is_operational = True,
            is_available = True,
        )
        entity = self.get_entity()
        if entity != 0:
            filters['entity'] = entity
        queryset = self.model.objects.filter(**filters)
        return queryset


class DispatchVehicleListView(OccurrenceAssociated):
    model = Vehicle
    form = VehicleSearchForm
    template_name = 'incident/dispatch_choose_vehicle.html'

    def get(self, request, occurrence_pk, entity_pk, **kw):
        self.occurrence = Occurrence.objects.get(pk=occurrence_pk)
        self.entity = Entity.objects.get(pk=entity_pk)
        return super(DispatchVehicleListView, self).get(request, occurrence_pk, **kw)

    def get_context_data(self, **kwargs):
        context_data = super(DispatchVehicleListView, self).get_context_data(**kwargs)
        last_week = datetime.now() - timedelta(7)
        context_data['occurrence'] = self.occurrence
        context_data['entity'] = self.entity
        context_data['search_form'] = form = self.form(self.request.GET)
        form.is_valid()
        form.update_fields()
        return context_data

    def get_queryset(self):
        log.error('%s %s' % (self.__class__.__name__, self.request.GET))
        filters = dict(
            is_operational = True,
            is_available = True,
            entity = self.entity,
        )
        form = self.form(self.request.GET)
        form.is_valid()
        filters.update(form.get_filters())
        queryset = self.model.objects.filter(**filters)
        return queryset


class OccurrenceDispatchCreateView(OwnedCreateView):
    model = Dispatch

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.occurrence = self.occurrence
        self.object.vehicle = Vehicle.objects.get(pk=self.request.GET['vehicle'])
        # Modify states
        self.object.vehicle.is_available = False
        self.object.vehicle.save()
        self.object.entity = self.object.vehicle.entity
        self.object.save()
        add_entry(self.request, self.object)
        if self.occurrence.status == Status.objects.get(pk=1) and \
           self.occurrence.dispatch_set.count() == 1:
            self.occurrence.status = Status.objects.get(pk=2)
            self.occurrence.save()
        return HttpResponseRedirect(self.get_success_url())

    def get(self, request, occurrence_pk, pk=None):
        self.occurrence = Occurrence.objects.get(pk=occurrence_pk)
        return super(OwnedCreateView, self).get(self, request, pk=pk)

    def get_context_data(self, **kwargs):
        context_data = super(OccurrenceDispatchCreateView, self).get_context_data(**kwargs)
        context_data['occurrence'] = self.occurrence
        return context_data

    def get_success_url(self):
        return urlresolvers.reverse(
            'dispatch_edit', args=[self.object.occurrence.pk, self.object.pk])

    def post(self, request, occurrence_pk, pk=None):
        self.occurrence = Occurrence.objects.get(pk=occurrence_pk)
        return super(OwnedCreateView, self).post(self, request, pk=pk)


class OccurrenceDispatchUpdateView(AutoSaveUpdateView):
    model = Dispatch

    def get(self, request, occurrence_pk, pk):
        self.occurrence = Occurrence.objects.get(pk=occurrence_pk)
        return super(OccurrenceDispatchUpdateView, self).get(request, pk=pk)

    def get_context_data(self, **kwargs):
        context_data = super(self.__class__, self).get_context_data(**kwargs)
        context_data['page_title'] = u'%s %s' % (_(u'Occurrence'), self.object)
        context_data['occurrence'] = self.occurrence
        return context_data


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
