__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@bloodpet.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from django.utils.translation import ugettext_lazy as _lazy, ugettext as _
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required

from incident.views import *


urlpatterns = patterns(
    'incident.views',
    url(r'^$', login_required(HomeView.as_view()),
        {'template': 'home.html'}, name='home'),
    url(r'^gotopage/$', login_required(GotoPage.as_view()),
        name='gotopage'),
    url(r'^create_associated/$', login_required(CreateAssociated.as_view()),
        name='create_associated'),
    # Alert URLs
    url(r'^alert/create/$',
        login_required(AlertCreateView.as_view()),
        name='alert_create'),
    url(r'alert/read/(?P<pk>\d+)/$',
        login_required(AlertDetailView.as_view()),
        name='alert_details'),
    url(r'alert/edit/(?P<pk>\d+)/$',
        login_required(AlertUpdateView.as_view()),
        name='alert_edit'),
    url(r'alert/associate/(?P<pk>\d+)/$',
        login_required(AlertAssociateView.as_view()),
        name='alert_associate'),
    url(r'^associate_alerts/$',
        login_required(AlertListAssociateView.as_view()),
        name='associate_alerts'),
    url(r'^alert/$',
        login_required(AlertListView.as_view()),
        name='alert_list'),
    # Occurrence URLs
    url(r'^occurrence/create/$',
        login_required(OccurrenceCreateView.as_view()),
        name='occurrence_create'),
    url(r'occurrence/read/(?P<pk>\d+)/$',
        login_required(OccurrenceDetailView.as_view()),
        name='occurrence_details'),
    url(r'occurrence/edit/(?P<pk>\d+)/$',
        login_required(OccurrenceUpdateView.as_view()),
        name='occurrence_edit'),
    url(r'occurrence/associate/(?P<pk>\d+)/$',
        login_required(OccurrenceAssociateView.as_view()),
        name='occurrence_associate'),
    url(r'occurrence/alerts/(?P<pk>\d+)/$',
        login_required(OccurrenceAlertsView.as_view()),
        name='occurrence_alerts'),
    url(r'occurrence/occurrences/(?P<pk>\d+)/$',
        login_required(OccurrenceOccurrencesView.as_view()),
        name='occurrence_occurrences'),
    url(r'^occurrence/$',
        login_required(OccurrenceListView.as_view()),
        name='occurrence_list'),
    url(r'^associate_occurrences/$',
        login_required(OccurrenceListAssociateView.as_view()),
        name='associate_occurrences'),
    url(r'occurrence/(?P<pk>\d+)/dispatches/$',
        login_required(OccurrenceDispatchListView.as_view()),
        name='dispatches'),
    url(r'occurrence/(?P<occurrence_pk>\d+)/dispatch/$',
        login_required(DispatchEntityListView.as_view()),
        name='dispatch_choose_entity'),
    url(r'occurrence/(?P<occurrence_pk>\d+)/dispatch/(?P<entity_pk>\d+)$',
        login_required(DispatchVehicleListView.as_view()),
        name='dispatch_choose_vehicle'),
    url(r'occurrence/(?P<occurrence_pk>\d+)/dispatch/create$',
        login_required(OccurrenceDispatchCreateView.as_view()),
        name='dispatch_create'),
    url(r'occurrence/(?P<occurrence_pk>\d+)/dispatch/edit/(?P<pk>\d+)$',
        login_required(OccurrenceDispatchUpdateView.as_view()),
        name='dispatch_edit'),
)


# vim: set noexpandtab ts=4 sw=4
