# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Importance'
        db.create_table('incident_importance', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_tl', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True)),
        ))
        db.send_create_signal('incident', ['Importance'])

        # Adding model 'Status'
        db.create_table('incident_status', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_tl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal('incident', ['Status'])

        # Adding model 'NatureFamily'
        db.create_table('incident_naturefamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_tl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('abbr', self.gf('django.db.models.fields.CharField')(max_length=16)),
        ))
        db.send_create_signal('incident', ['NatureFamily'])

        # Adding model 'NatureType'
        db.create_table('incident_naturetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('nature_family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['incident.NatureFamily'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_tl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('abbr', self.gf('django.db.models.fields.CharField')(max_length=16)),
        ))
        db.send_create_signal('incident', ['NatureType'])

        # Adding model 'Nature'
        db.create_table('incident_nature', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('nature_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['incident.NatureType'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_tl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal('incident', ['Nature'])

        # Adding model 'Source'
        db.create_table('incident_source', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_tl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal('incident', ['Source'])

        # Adding model 'SourceType'
        db.create_table('incident_sourcetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_tl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal('incident', ['SourceType'])

        # Adding model 'Alert'
        db.create_table('incident_alert', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('occurrence', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['incident.Occurrence'], null=True, blank=True)),
            ('start_ts', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('end_ts', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('nature', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['incident.Nature'])),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['incident.Source'], null=True, blank=True)),
            ('source_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['incident.SourceType'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.District'], null=True, blank=True)),
            ('county', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.County'], null=True, blank=True)),
            ('locality', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.Locality'], null=True, blank=True)),
            ('local', self.gf('django.db.models.fields.CharField')(max_length=256, blank=True)),
            ('point_of_reference', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('latitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('longitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
        ))
        db.send_create_signal('incident', ['Alert'])

        # Adding model 'Occurrence'
        db.create_table('incident_occurrence', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('occurrence', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='associated', null=True, to=orm['incident.Occurrence'])),
            ('start_ts', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('end_ts', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('importance', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['incident.Importance'])),
            ('status', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['incident.Status'])),
            ('nature', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['incident.Nature'])),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.Entity'], null=True, blank=True)),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.District'], null=True, blank=True)),
            ('county', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.County'], null=True, blank=True)),
            ('locality', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.Locality'], null=True, blank=True)),
            ('local', self.gf('django.db.models.fields.CharField')(max_length=256, blank=True)),
            ('point_of_reference', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('latitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('longitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
        ))
        db.send_create_signal('incident', ['Occurrence'])

        # Adding model 'ResourceType'
        db.create_table('incident_resourcetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('incident', ['ResourceType'])

        # Adding model 'Resource'
        db.create_table('incident_resource', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('resource_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['incident.ResourceType'])),
        ))
        db.send_create_signal('incident', ['Resource'])

        # Adding model 'DispatchStatus'
        db.create_table('incident_dispatchstatus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_tl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal('incident', ['DispatchStatus'])

        # Adding model 'Dispatch'
        db.create_table('incident_dispatch', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('occurrence', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['incident.Occurrence'])),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.Entity'], null=True, blank=True)),
            ('vehicle', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['operation_resource.Vehicle'], null=True, blank=True)),
            ('start_ts', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('exit_hq_ts', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('arrive_to_ts', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('exit_to_ts', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('arrive_hq_ts', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['incident.DispatchStatus'], null=True, blank=True)),
            ('observations', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('incident', ['Dispatch'])


    def backwards(self, orm):
        # Deleting model 'Importance'
        db.delete_table('incident_importance')

        # Deleting model 'Status'
        db.delete_table('incident_status')

        # Deleting model 'NatureFamily'
        db.delete_table('incident_naturefamily')

        # Deleting model 'NatureType'
        db.delete_table('incident_naturetype')

        # Deleting model 'Nature'
        db.delete_table('incident_nature')

        # Deleting model 'Source'
        db.delete_table('incident_source')

        # Deleting model 'SourceType'
        db.delete_table('incident_sourcetype')

        # Deleting model 'Alert'
        db.delete_table('incident_alert')

        # Deleting model 'Occurrence'
        db.delete_table('incident_occurrence')

        # Deleting model 'ResourceType'
        db.delete_table('incident_resourcetype')

        # Deleting model 'Resource'
        db.delete_table('incident_resource')

        # Deleting model 'DispatchStatus'
        db.delete_table('incident_dispatchstatus')

        # Deleting model 'Dispatch'
        db.delete_table('incident_dispatch')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'incident.alert': {
            'Meta': {'object_name': 'Alert'},
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']", 'null': 'True', 'blank': 'True'}),
            'end_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'local': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'locality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Locality']", 'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'nature': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['incident.Nature']"}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'occurrence': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['incident.Occurrence']", 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'point_of_reference': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['incident.Source']", 'null': 'True', 'blank': 'True'}),
            'source_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['incident.SourceType']", 'null': 'True', 'blank': 'True'}),
            'start_ts': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'incident.dispatch': {
            'Meta': {'object_name': 'Dispatch'},
            'arrive_hq_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'arrive_to_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Entity']", 'null': 'True', 'blank': 'True'}),
            'exit_hq_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'exit_to_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'observations': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'occurrence': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['incident.Occurrence']"}),
            'start_ts': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['incident.DispatchStatus']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'vehicle': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Vehicle']", 'null': 'True', 'blank': 'True'})
        },
        'incident.dispatchstatus': {
            'Meta': {'object_name': 'DispatchStatus'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_tl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'incident.importance': {
            'Meta': {'object_name': 'Importance'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_tl': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True'})
        },
        'incident.nature': {
            'Meta': {'object_name': 'Nature'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_tl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'nature_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['incident.NatureType']"}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'incident.naturefamily': {
            'Meta': {'object_name': 'NatureFamily'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_tl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'incident.naturetype': {
            'Meta': {'object_name': 'NatureType'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_tl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'nature_family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['incident.NatureFamily']"}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'incident.occurrence': {
            'Meta': {'object_name': 'Occurrence'},
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']", 'null': 'True', 'blank': 'True'}),
            'end_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Entity']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importance': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['incident.Importance']"}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'local': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'locality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Locality']", 'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'nature': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['incident.Nature']"}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'occurrence': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'associated'", 'null': 'True', 'to': "orm['incident.Occurrence']"}),
            'point_of_reference': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'start_ts': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['incident.Status']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'incident.resource': {
            'Meta': {'object_name': 'Resource'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'resource_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['incident.ResourceType']"})
        },
        'incident.resourcetype': {
            'Meta': {'object_name': 'ResourceType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'incident.source': {
            'Meta': {'object_name': 'Source'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_tl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'incident.sourcetype': {
            'Meta': {'object_name': 'SourceType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_tl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'incident.status': {
            'Meta': {'object_name': 'Status'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_tl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'location.county': {
            'Meta': {'object_name': 'County'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'location.district': {
            'Meta': {'object_name': 'District'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'location.locality': {
            'Meta': {'object_name': 'Locality'},
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'operation_resource.agency': {
            'Meta': {'object_name': 'Agency'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True'})
        },
        'operation_resource.entity': {
            'Meta': {'object_name': 'Entity'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'agency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Agency']", 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.EntityCategory']", 'null': 'True', 'blank': 'True'}),
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']", 'null': 'True', 'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Locality']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.EntityType']", 'null': 'True', 'blank': 'True'})
        },
        'operation_resource.entitycategory': {
            'Meta': {'object_name': 'EntityCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'operation_resource.entitytype': {
            'Meta': {'object_name': 'EntityType'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.EntityCategory']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'operation_resource.vehicle': {
            'Meta': {'object_name': 'Vehicle'},
            'code': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.VehicleCode']", 'null': 'True', 'blank': 'True'}),
            'county': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.County']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.District']", 'null': 'True', 'blank': 'True'}),
            'end_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.Entity']", 'null': 'True', 'blank': 'True'}),
            'garrison': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_operational': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'license': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'locality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Locality']", 'null': 'True', 'blank': 'True'}),
            'mark': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'start_ts': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'operation_resource.vehiclecategory': {
            'Meta': {'object_name': 'VehicleCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'operation_resource.vehiclecode': {
            'Meta': {'object_name': 'VehicleCode'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.VehicleType']", 'null': 'True', 'blank': 'True'})
        },
        'operation_resource.vehicletype': {
            'Meta': {'object_name': 'VehicleType'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['operation_resource.VehicleCategory']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['incident']