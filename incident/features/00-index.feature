Feature: Incident Index
	Manage Calls, Alerts and Occurrences.
	Each occurrence could contain one or more alerts.
	Each call will create an alert.

	Scenario Outline: Home - Ongoing page
		When I login as "eman" with password "123456"
		And I visit "home"
		Then I see the header "Ongoing"
		And I see the #<number> "h3" element with text "<text>"

	Examples:
		| number | text |
		| 1 | Open alerts this week |
		| 2 | Alerts not associated with occurrences |
	
	Scenario: Open alerts
		Given I create a test alert
		And I create a test request
		When I visit "home"
		Then I see the #1 "table#open-alerts thead tr th" element with text "Alert"
		And I see the #1 "table#open-alerts tbody tr td" element with text "1"
		And I see the #1 "table#unassociated-alerts thead tr th" element with text "Alert"
		And I see the #1 "table#unassociated-alerts tbody tr td" element with text "1"
