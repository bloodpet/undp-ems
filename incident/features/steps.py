__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@bloodpet.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from lettuce import *


@step(u'create a test alert')
def create_a_test_alert(step):
    from django.contrib.auth.models import User
    from incident.models import Alert, Source, Status, Nature
    alert = Alert(
        user=User.objects.get(pk=1),
        nature=Nature.objects.get(pk=1),
    )
    alert.save()
    return alert


@step(u'create a test occurrence')
def create_a_test_occurrence(step):
    from django.contrib.auth.models import User
    from incident.models import Occurrence, Source
    occurrence = Occurrence(
        title='Test',
        dispatch_code='',
        user=User.objects.get(pk=1),
        source=Source.objects.get(pk=1),
    )
    occurrence.save()
    return occurrence


@step(u'create a test resource')
def create_a_test_resource(step):
    from django.contrib.auth.models import User
    from incident.models import Resource, ResourceType
    resource = Resource(
        name='Test',
        resource_type=ResourceType.objects.get(pk=1),
    )
    resource.save()
    return resource


@step(u'create a test request')
def create_a_test_request(step):
    from incident.models import DispatchRequest
    occurrence = create_a_test_occurrence(step)
    request = DispatchRequest(
        name='Test',
        occurrence=occurrence,
    )
    request.save()
    return request


# vim: set noexpandtab ts=4 sw=4
