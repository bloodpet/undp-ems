Feature: Dispatch
	Manage Dispatches

    Scenario: Create Dispatch Request
		When I login as "eman" with password "123456"
		And I visit "dispatch_request_create"
		And I see the form with id "dispatch_request-form"
		And "dispatch_request-form" form has fields:
			| name | type |
			| name | text |
			| occurrence | dropdown |
		Given I create a test occurrence
		When i submit "dispatch_request-form" form with values:
			| name | value |
			| name | Test |
			| occurrence | 1 |
		Then check if entry is saved

	Scenario: Read Dispatch Request
		When I visit "dispatch_request_list"
		And I see a link in the content to "dispatch_request_details 1"
		Given I visit "dispatch_request_details 1"
		Then I see a link in the content to "dispatch_request_list"
		And I see a link in the content to "dispatch_request_edit 1"

	Scenario: List Dispatch Requests
		When I visit "dispatch_request_list"
		Then I see the header "Dispatch Request"
		And I see the #1 "table tbody tr td" element with text "Test"


    Scenario: Create Dispatch
		When I login as "eman" with password "123456"
		And I visit "dispatch_create"
		And I see the form with id "dispatch-form"
		And "dispatch-form" form has fields:
			| name | type |
			| name | text |
			| resource | dropdown |
			| dispatch_request | dropdown |
		Given I create a test resource
		When i submit "dispatch-form" form with values:
			| name | value |
			| name | Test |
			| resource | 1 |
			| dispatch_request | 1 |
		Then check if entry is saved

	Scenario: Read Dispatch
		When I visit "dispatch_list"
		And I see a link in the content to "dispatch_details 1"
		Given I visit "dispatch_details 1"
		Then I see a link in the content to "dispatch_list"
		And I see a link in the content to "dispatch_edit 1"

	Scenario: List Dispatches
		When I visit "dispatch_list"
		Then I see the header "Dispatch"
		And I see the #1 "table tbody tr td" element with text "Test"


# vim: set noexpandtab ts=4 sw=4
