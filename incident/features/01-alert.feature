Feature: Alerts
	Manage Alerts

	Scenario: Alert Index
		Given I login as "eman" with password "123456"
		Then I visit "alert_list"

	Scenario: Create Alert
		Given I login as "eman" with password "123456"
		And I visit "alert_create"
		And I see the header "Create Alert"
		And I see the form with id "alert-form"
		And "alert-form" form has fields:
			| name | type |
			| nature | dropdown |
		When i submit "alert-form" form with values:
			| name | value |
			| nature | 1 |
		Then check if entry is saved.

	Scenario: List Alerts
		When I visit "alert_list"
		Then I see the header "Alert"
		And I see the #1 "table tbody tr td" element with text "1"
		And I see the #4 "table tbody tr td" element with text "1100 INC / POV"
	
	#Scenario: Read Alert
		#When I visit "alert_list"
		#And I see a link in the content to "alert_details 1"
		#Given I visit "alert_details 1"
		#Then I see a link in the content to "alert_list"
		#And I see a link in the content to "alert_edit 1"

	Scenario: Edit Alert
		Given I visit "alert_list"
		And I see a link in the content to "alert_edit 1"
		When I visit "alert_edit 1"
		Then I see the header "Edit Alert"
		And "alert-form" form has fields:
			| name | type |
			| nature | dropdown |

	Scenario: Associate Occurrence
		Given I visit "alert_edit 1"
		#And I create a test occurrence
		#When I see a link in the content to "alert_associate 1"


# vim: set noexpandtab ts=4 sw=4
