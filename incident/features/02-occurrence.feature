Feature: Occurrence
	Manage Occurrences

	Scenario: Create Occurrence
		When I login as "eman" with password "123456"
		And I visit "occurrence_create"
		Then I see the header "Create Occurrence"
		And I see the form with id "occurrence-form"
		And "occurrence-form" form has fields:
			| name | type |
			| title | text |
			| dispatch_code | text |
			| source | dropdown |
		When i submit "occurrence-form" form with values:
			| name | value |
			| title | Test |
			| dispatch_code | No dispatch code |
			| source | 1 |
		Then check if entry is saved

	Scenario: Read Occurrence
		When I visit "occurrence_list"
		And I see a link in the content to "occurrence_details 1"
		Given I visit "occurrence_details 1"
		Then I see a link in the content to "occurrence_list"
		And I see a link in the content to "occurrence_edit 1"

	Scenario: List Occurrences
		When I visit "occurrence_list"
		Then I see the header "Occurrence"
		And I see the #1 "table tbody tr td" element with text "Test"


# vim: set noexpandtab ts=4 sw=4
