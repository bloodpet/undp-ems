Feature: Resource
	Manage Resources

    Scenario: Create Resource
		When I login as "eman" with password "123456"
		And I visit "resource_create"
		And I see the form with id "resource-form"
		And "resource-form" form has fields:
			| name | type |
			| name | text |
			| resource_type | dropdown |
		When i submit "resource-form" form with values:
			| name | value |
			| name | Test |
			| resource_type | 1 |
		Then check if entry is saved

	Scenario: Read Resource
		When I visit "resource_list"
		And I see a link in the content to "resource_details 1"
		Given I visit "resource_details 1"
		Then I see a link in the content to "resource_list"
		And I see a link in the content to "resource_edit 1"

	Scenario: List Resources
		When I visit "resource_list"
		Then I see the header "Resource"
		And I see the #1 "table tbody tr td" element with text "Test"


# vim: set noexpandtab ts=4 sw=4
