__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from modeltranslation.translator import translator, TranslationOptions
from incident.models import (
    DispatchStatus, Importance, Nature, NatureFamily, NatureType, Source, Status, SourceType,
)


class DispatchStatusTranslationOptions(TranslationOptions):
    fields = ('name', )


class ImportanceTranslationOptions(TranslationOptions):
    fields = ('name', )


class NatureTranslationOptions(TranslationOptions):
    fields = ('name', )


class NatureFamilyTranslationOptions(TranslationOptions):
    fields = ('name', )


class NatureTypeTranslationOptions(TranslationOptions):
    fields = ('name', )


class SourceTranslationOptions(TranslationOptions):
    fields = ('name', )


class SourceTypeTranslationOptions(TranslationOptions):
    fields = ('name', )


class StatusTranslationOptions(TranslationOptions):
    fields = ('name', )


translator.register(DispatchStatus, DispatchStatusTranslationOptions)
translator.register(Importance, ImportanceTranslationOptions)
translator.register(Nature, NatureTranslationOptions)
translator.register(NatureFamily, NatureFamilyTranslationOptions)
translator.register(NatureType, NatureTypeTranslationOptions)
translator.register(Source, SourceTranslationOptions)
translator.register(SourceType, SourceTypeTranslationOptions)
translator.register(Status, StatusTranslationOptions)


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
