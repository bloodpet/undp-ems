__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline
from incident.models import (
    DispatchStatus, Importance, Nature, NatureFamily, NatureType, Source, Status, SourceType,
)


class NatureInline(TranslationTabularInline):
    model = Nature


class NatureTypeInline(TranslationTabularInline):
    model = NatureType


class GenericTranslationAdmin(TranslationAdmin):
    list_display = ('__unicode__', 'name_pt', 'name_tl', 'name_en', )


class NatureFamilyAdmin(GenericTranslationAdmin):
    inlines = [NatureTypeInline, ]


class NatureTypeAdmin(GenericTranslationAdmin):
    inlines = [NatureInline, ]


admin.site.register(DispatchStatus, GenericTranslationAdmin)
admin.site.register(Importance, GenericTranslationAdmin)
admin.site.register(Nature, GenericTranslationAdmin)
admin.site.register(NatureFamily, NatureFamilyAdmin)
admin.site.register(NatureType, NatureTypeAdmin)
admin.site.register(Source, GenericTranslationAdmin)
admin.site.register(SourceType, GenericTranslationAdmin)
admin.site.register(Status, GenericTranslationAdmin)


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
