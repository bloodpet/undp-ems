__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline
from location.models import (
    District, County, Locality
)


class CountyInline(admin.TabularInline):
    model = County


class LocalityInline(admin.TabularInline):
    model = Locality


class DistrictAdmin(admin.ModelAdmin):
    inlines = [CountyInline, ]


class CountyAdmin(admin.ModelAdmin):
    inlines = [LocalityInline, ]


class LocalityAdmin(admin.ModelAdmin):
    pass


admin.site.register(District, DistrictAdmin)
admin.site.register(County, CountyAdmin)
admin.site.register(Locality, LocalityAdmin)


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
