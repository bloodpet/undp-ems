__LICENSE__ = '''Copyright (C) 2012 "Emanuel Calso" <eman@xaasy.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see
http://www.gnu.org/licenses/agpl-3.0.html.
'''


from django.db import models
from django.utils.translation import ugettext_lazy as _lazy, ugettext as _
from django.utils.encoding import smart_unicode


class District(models.Model):
    name = models.CharField(_(u'Name'), max_length=256)
    number = models.PositiveIntegerField(
        _(u'Number'), serialize=True,
        auto_created=True)

    class Meta:
        verbose_name = _(u'District')
        verbose_name_plural = _(u'Districts')
        verbose_name = _lazy(u'District')
        verbose_name_plural = _lazy(u'Districts')

    def __unicode__(self):
        return u'%0*d %s' % (2, self.number, smart_unicode(self.name))


class County(models.Model):
    district = models.ForeignKey(District, verbose_name=_(u'District'))
    name = models.CharField(_(u'Name'), max_length=256)
    number = models.PositiveIntegerField(
        _(u'Number'), serialize=True,
        auto_created=True)

    class Meta:
        verbose_name = _(u'Subdistrict')
        verbose_name_plural = _(u'Subdistricts')
        verbose_name = _lazy(u'Subdistrict')
        verbose_name_plural = _lazy(u'Subdistricts')

    def __unicode__(self):
        return u'%0*d%0*d %s' % (2, self.district.number, 2, self.number, smart_unicode(self.name))


class Locality(models.Model):
    county = models.ForeignKey(County, verbose_name=_(u'Subdistrict'))
    name = models.CharField(_(u'Name'), max_length=256)
    number = models.PositiveIntegerField(
        _(u'Number'), serialize=True,
        auto_created=True)

    class Meta:
        verbose_name = _(u'Suco')
        verbose_name_plural = _(u'Suco')
        verbose_name = _lazy(u'Suco')
        verbose_name_plural = _lazy(u'Suco')

    def __unicode__(self):
        return u'%0*d%0*d%0*d %s' % (
            2, self.county.district.number,
            2, self.county.number,
            2, self.number,
            smart_unicode(self.name))
