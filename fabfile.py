#!/usr/bin/env python
#fabfile.py
from __future__ import with_statement

from fabric.api import *


env.hosts = ['xaasy@xarg.us', ]
home = '/home/xaasy'
project_dir = home + '/undp_ems'
django_dir = project_dir + '/undp_ems'
remote_db = django_dir + '/default.db'
logs_dir = home + '/logs/user/'
dev_remote = 'origin'
dev_branch = 'master'
prod_remote = 'xaasy'
prod_branch = 'master'


def update():
    local('git push origin %s:master &' % (
        prod_branch,
    ))
    local('git push %s %s:master' % (
        prod_remote,
        prod_branch,
    ))
    with cd(django_dir):
        run('git pull origin master')


def update_master():
    """docstring for deploy"""
    local('git push %s %s' % (
        dev_remote,
        dev_branch,
    ))
    local('git checkout %s' % prod_branch)
    local('git merge %s' % dev_branch)
    update()
    local('git checkout %s' % dev_branch)


def restart():
    with cd(project_dir):
        run('./apache2/bin/restart')


def deploy():
    """Deploy production branch"""
    local('git checkout %s' % prod_branch)
    update()
    restart()
    local('git checkout master')


def deploy_master():
    """Deploy master branch"""
    update_master()
    restart()
    local('git checkout master')


def git(cmd):
    with cd(django_dir):
        run('git %s' % cmd)


def resync_remote():
    with cd(django_dir):
        run('python2.7 manage.py syncdb --noinput')


def repopulate_local():
    local('rm test.db')
    local('python manage.py syncdb --noinput')
    local('python manage.py loaddata local_data.json')
    local('python manage.py create_sections')


def repopulate_remote():
    with cd(django_dir):
        try:
            run('rm %s' % remote_db)
        except Exception:
            pass
        run('python2.7 manage.py syncdb --noinput')
        run('python2.7 manage.py loaddata local_data.json')
        run('python2.7 manage.py create_sections')


def view_error_log(n=10):
    with cd(logs_dir):
        run('tail -n %s error_road_grid.log' % n)


def view_access_log(n=10):
    with cd(logs_dir):
        run('tail -n %s access_road_grid.log' % n)


def copy_logs():
    local(
        'scp -Crv %(host)s:%(logs_dir)s/*road_grid* %(dest)s' % dict(
            dest='res/logs/',
            host=env.hosts[0],
            logs_dir=logs_dir,
        )
    )


def rebase_branches():
    cmd = '''
    for b in `git branch | sed 's/.* //'`;
    do if [ $b == master ];
    then echo $b;
    else echo "";
    fi;
    done
    '''
    local(cmd)


def scp(*args):
    for a in args:
        local(
            'scp -Crv %(filename)s %(host)s:%(project_dir)s/%(filename)s' %
            dict(
                filename=a,
                host=env.hosts[0],
                project_dir=project_dir,
            )
        )
copy_files = scp


def scp_from_server(*args):
    for a in args:
        local(
            'scp -Crv %(host)s:%(project_dir)s/%(filename)s %(filename)s' %
            dict(
                filename=a,
                host=env.hosts[0],
                project_dir=project_dir,
            )
        )


def parse_roads():
    with cd(django_dir):
        run('python2.7 manage.py parse_roads')


def syncdb():
    with cd(django_dir):
        run('python2.7 manage.py syncdb')


def get_tweets():
    with cd(django_dir):
        run('python2.7 manage.py get_tweets')
        run('python2.7 manage.py parse_tweets')


def parse_tweets():
    with cd(django_dir):
        run('python2.7 manage.py parse_tweets')


def parse_tv5(*args):
    with cd(django_dir):
        run('python2.7 manage.py parse_tv5 %s' % ' '.join(args))


# vim: set expandtab ts=4 filetype=python fileencoding=utf-8 ai
